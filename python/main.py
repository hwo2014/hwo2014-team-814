from __future__ import print_function
import json
import socket
import sys
import math

def some(func, items):
    return len(filter(None, map(func, items))) > 0

class Piece(object):

    def __init__(self, piece):
        self._index = piece['index']
        self._velocity = piece['velocity']

        self._isSwitch = False
        if 'switch' in piece.keys():
            self._isSwitch = True

    def is_switch_lane(self):
        return self._isSwitch

    def get_index(self):
        return self._index

    def set_max_velocity(self, velocity):
        if velocity < 0:
            velocity = 0.1
        self._velocity = velocity

    def get_max_velocity(self):
        return self._velocity

    def increase_max_velocity(self, amount = 0.1):
        self._velocity = self._velocity + amount

    def decrease_max_velocity(self, amount = 0.1):
        self._velocity = self._velocity - amount

class Straight(Piece):

    def __init__(self, piece):
        Piece.__init__(self, piece)
        self._length = float(piece['length'])
        self.set_max_velocity(10)

    def __len__(self):
        return self._length

    def __str__(self):
        return "length: %s, isSwitch: %s" % (len(self), self.is_switch_lane())

class Curve(Piece):
    
    def __init__(self, piece):
        Piece.__init__(self, piece)
        self.set_angle(piece)
        self.set_radius(piece)
        # This is most scientific method to determine curve max velocity. 
        # Cracking this formula has take many nights to work.
        # Mostly harrison-stetson method is used to determine this.
        velocity = 10 - (( math.fabs(self.get_angle()) / len(self)) + math.fabs(self.get_angle()) / 10 ) 
        if velocity > 7:
            velocity = 7
        self.set_max_velocity(velocity)

    def set_angle(self, piece):
        self._angle = float(piece['angle'])

    def get_angle(self):
        return self._angle

    def set_radius(self, piece):
        self._radius = float(piece['radius'])

    def get_radius(self):
        return self._radius

    def is_clockwise(self):
        return self._angle > 0

    def __len__(self):
        return math.fabs((self.get_angle() * math.pi * self.get_radius()) / 180)

    def __str__(self):
        return "angle: %s, isSwitch: %s" % (self.get_angle(), self.is_switch_lane())

class Track(object):

    def __init__(self, pieces, lanes):
        self._pieces = pieces
        self._lanes = lanes

    def get_next_pieces(self, index, count = 3):
        following_piece_indexes = \
            [(index + i) % len(self._pieces) for i in xrange(1, count + 1)]

        return [p for i, p in enumerate(self._pieces) 
                if i in following_piece_indexes]

    def get_piece_by_index(self, index):
        return self._pieces[index]
    
    def following_pieces_has_angle(self, index, count = 3, angle = 45):
        return [p for p in self.get_next_pieces(index, count)
                if isinstance(p, Curve) and math.fabs(p.get_angle()) >= angle]

    def get_lanes(self):
        return self._lanes

    def get_lane_by_index(self, index):
        return self._lanes[index % len(self._lanes)]

    def is_more_curves_clockwise(self, index):
        pieces = self.get_next_pieces(index, 4)
        curves = filter(lambda p: isinstance(p, Curve), pieces)
        clockwise = filter(lambda c: c.get_angle() > 0, curves)
        anti_clockwise = filter(lambda c: c.get_angle() < 0, curves)

        return len(clockwise) >= len(anti_clockwise)

    def get_next_curve(self, index):
        begin = self._pieces[:index]
        end = self._pieces[index:]

        for p in end + begin:
            if isinstance(p, Curve):
                return p
        return None

    def is_next_curve_clockwise(self, index):
        return self.get_next_curve(index).get_angle() > 0

class Car(object):

    def __init__(self):
        self._current_lane = None
        self._throttle = 1
        self._angle = 0.0
        self._velocity = 0.0
        self._in_piece_distance = 0.0
        self._previous_angle = 0.0
        self._max_angle = 40

    def set_max_angle(self, angle):
        self._max_angle = math.fabs(angle)

    def get_max_angle(self):
        return self._max_angle

    def set_throttle(self, throttle):
        self._throttle = throttle

    def get_throttle(self):
        return self._throttle

    def increase_throttle(self, amount = 0.1):
        self._throttle = self._throttle + amount
        if self._throttle > 1:
            self._throttle = 1

    def decrease_throttle(self, amount = 0.1):
        self._throttle = self._throttle - amount
        if self._throttle < 0:
            self._throttle = 0

    def set_current_lane(self, lane):
        self._current_lane = lane

    def set_in_piece_distance(self, distance):
        # piece has changed
        if distance < self._in_piece_distance:
            self._in_piece_distance = 0.0
        self._velocity = math.fabs(distance - self._in_piece_distance)
        self._in_piece_distance = distance

    def get_in_piece_distance(self):
        return self._in_piece_distance

    def get_current_lane(self):
        return self._current_lane

    def is_current_lane_most_inner(self, lanes):
        for lane in lanes:
            if self._current_lane > lane:
                return True
        return False

    def set_angle(self, angle):
        self._previous_angle = self._angle
        self._angle = float(angle)

    def get_angle(self):
        return self._angle

    def get_velocity(self):
        return self._velocity

class Lane(object):

    def __init__(self, index):
        self.lane_index = index

    def __gt__(self, other):
        return self.lane_index > other.get_lane_index()

    def __lt__(self, other):
        return self.lane_index < other.get_lane_index()

    def get_lane_index(self):
        return self.lane_index


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.current_piece = None
        self.car = Car()

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def turbo_on(self):
        self.msg("turbo", "on")

    def join(self):
        return self.msg("join", {"name": self.name, "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def switch_lane(self, lane):
        self.msg('switchLane', lane)

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.throttle(1)

    def on_car_positions(self, data):
        my_data = filter(lambda x: x['id']['name'] == self.name, data)[0]

        current_piece = my_data['piecePosition']
        index = current_piece['pieceIndex']
        self.current_piece = self.track.get_piece_by_index(index)
    
        self.car.set_current_lane(self.track.get_lane_by_index(current_piece['lane']['endLaneIndex']))
        self.car.set_in_piece_distance(current_piece['inPieceDistance'])
        self.car.set_angle(data[0]['angle'])

        following_pieces = self.track.get_next_pieces(index, 3)
        print("velocity %s :: angle %s :: throttle %s" % (self.car.get_velocity(), self.car.get_angle(), self.car.get_throttle()))

        if following_pieces[0].is_switch_lane() and self.car.get_velocity() > 0:
            if self.track.is_more_curves_clockwise(index):
                self.switch_lane('Right')
            else:
                self.switch_lane('Left')
        else:
            throttle = self.get_throttle(following_pieces)
            self.car.set_throttle(throttle)
            self.throttle(self.car.get_throttle())

    def get_throttle(self, following_pieces):
        if self.car.get_velocity < 2:
            return 1

        velocity = self.car.get_velocity()

        if isinstance(self.current_piece, Curve) \
           and velocity < self.current_piece.get_max_velocity() \
           and math.fabs(self.car.get_angle()) < self.car.get_max_angle():
            return 1
        elif math.fabs(self.car.get_angle()) > self.car.get_max_angle():
            return 0
        elif some(lambda p: isinstance(p, Curve) and velocity > p.get_max_velocity(), following_pieces):
            return 0
        elif some(lambda p: velocity < p.get_max_velocity(), following_pieces):
            return 1

        return 1

    def on_crash(self, data):
        self.current_piece.decrease_max_velocity(0.2)
        self.car.set_max_angle(self.car.get_max_angle() - 5)
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_game_init(self, data):
        track = data['race']['track']

        pieces = []
        for index, piece in enumerate(track['pieces']):
            piece['index'] = index
            piece['velocity'] = 6
            if 'angle' in piece.keys():
                pieces.append(Curve(piece))
            else:
                pieces.append(Straight(piece))

        lanes = []
        for lane in track['lanes']:
            lanes.append(Lane(lane['index']))

        self.track = Track(pieces, lanes)

        self.ping()

    def on_lap_finished(self, data):
        self.crashes = []

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'lapFinishes': self.on_lap_finished,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                if msg_type == 'turboAvailable':
                    self.turbo_available = True
                if msg_type in ['turboStart', 'turboEnd']:
                    self.turbo_available = False
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
